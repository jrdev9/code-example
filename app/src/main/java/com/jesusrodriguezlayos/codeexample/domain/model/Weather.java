package com.jesusrodriguezlayos.codeexample.domain.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class Weather {

  @SerializedName("id")
  private String idCity;

  @SerializedName("name")
  private String nameCity;

  @SerializedName("main")
  private AtmosphericConditions atmosphericConditions;

  @Override
  public String toString() {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    return gson.toJson(this);
  }

  public String getIdCity() {
    return idCity;
  }

  public String getNameCity() {
    return nameCity;
  }

  public AtmosphericConditions getAtmosphericConditions() {
    return atmosphericConditions;
  }
}
