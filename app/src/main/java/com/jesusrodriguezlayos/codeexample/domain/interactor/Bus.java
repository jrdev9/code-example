package com.jesusrodriguezlayos.codeexample.domain.interactor;

import org.greenrobot.eventbus.EventBus;

public class Bus {

  public Bus() {

  }

  private static EventBus initialize() {
    return EventBus.getDefault();
  }

  public static void postSticky(Object object) {
    initialize().postSticky(object);
  }

  public static <T> T getStickyEvent(Class<T> objectClass) {
    return initialize().getStickyEvent(objectClass);
  }
}
