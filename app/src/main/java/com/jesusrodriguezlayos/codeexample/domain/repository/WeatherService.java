package com.jesusrodriguezlayos.codeexample.domain.repository;

import com.jesusrodriguezlayos.codeexample.domain.model.Weather;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

public interface WeatherService {
  @GET("weather")
  Call<Weather> weather(@Query("q") String city, @Query("units") String unit,
      @Query("appid") String apiKey);
}
