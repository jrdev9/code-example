package com.jesusrodriguezlayos.codeexample.domain.repository;

import android.support.annotation.NonNull;
import com.jesusrodriguezlayos.codeexample.domain.model.Weather;
import com.squareup.okhttp.OkHttpClient;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class WeatherRepository {

  private static final String ENDPOINT = "http://api.openweathermap.org/data/2.5/";
  private static final String UNIT = "metric";
  private static final String API_KEY = "bdbb0b46a436da51bcae16e02fd4e4db";

  private WeatherService api;
  private WeatherServiceActions weatherServiceActions;

  public interface WeatherServiceActions {
    void onResponse(Weather weather);

    void onFailure(Throwable error);
  }

  public WeatherRepository(@NonNull WeatherServiceActions weatherServiceActions) {
    this.weatherServiceActions = weatherServiceActions;
    initialize();
  }

  private void initialize() {
    Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT)
        .addConverterFactory(GsonConverterFactory.create())
        .client(buildClient())
        .build();

    this.api = retrofit.create(WeatherService.class);
  }

  private OkHttpClient buildClient() {
    return new OkHttpClient();
  }

  public void searchCurrentWeather(String param) {
    Call<Weather> weatherCall = getApi().weather(param, UNIT, API_KEY);
    weatherCall.enqueue(new Callback<Weather>() {
      @Override
      public void onResponse(Response<Weather> response, Retrofit retrofit) {
        weatherServiceActions.onResponse(response.body());
      }

      @Override
      public void onFailure(Throwable error) {
        weatherServiceActions.onFailure(error);
      }
    });
  }

  public WeatherService getApi() {
    if (api == null) {
      initialize();
    }
    return api;
  }
}
