package com.jesusrodriguezlayos.codeexample.domain.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class AtmosphericConditions {

  @SerializedName("temp")
  private String temperature;
  @SerializedName("pressure")
  private String preassure;
  @SerializedName("humidity")
  private String humidity;
  @SerializedName("temp_min")
  private String minimumTemperature;
  @SerializedName("temp_max")
  private String maximumTemperature;

  @Override
  public String toString() {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    return gson.toJson(this);
  }

  public String getTemperature() {
    return temperature;
  }

  public String getPreassure() {
    return preassure;
  }

  public String getHumidity() {
    return humidity;
  }

  public String getMinimumTemperature() {
    return minimumTemperature;
  }

  public String getMaximumTemperature() {
    return maximumTemperature;
  }
}
