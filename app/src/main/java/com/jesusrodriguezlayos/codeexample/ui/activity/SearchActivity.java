package com.jesusrodriguezlayos.codeexample.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.jesusrodriguezlayos.codeexample.R;
import com.jesusrodriguezlayos.codeexample.ui.presenter.SearchPresenter;
import com.jesusrodriguezlayos.codeexample.ui.view.SearchView;

public class SearchActivity extends MvpActivity<SearchView, SearchPresenter> implements SearchView {

  @Bind(R.id.edt_search_city)
  EditText edtSearchCity;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.search_view);
    ButterKnife.bind(this);
  }

  @NonNull
  @Override
  public SearchPresenter createPresenter() {
    return new SearchPresenter();
  }

  @OnClick(R.id.button_search)
  public void onClickSearch() {
    presenter.loadWeatherFromCity(edtSearchCity.getText().toString());
  }

  @Override
  public void openWeatherActivity() {
    Intent intent = new Intent(this, WeatherDetailActivity.class);
    startActivity(intent);
  }

  @Override
  public void showError(Throwable error) {
    Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
  }

  @Override
  public void emptySearchParam() {
    Toast.makeText(this, R.string.empty_field, Toast.LENGTH_SHORT).show();
  }
}
