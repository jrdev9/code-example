package com.jesusrodriguezlayos.codeexample.ui.presenter;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.jesusrodriguezlayos.codeexample.domain.interactor.Bus;
import com.jesusrodriguezlayos.codeexample.domain.model.Weather;
import com.jesusrodriguezlayos.codeexample.domain.repository.WeatherRepository;
import com.jesusrodriguezlayos.codeexample.ui.view.SearchView;

public class SearchPresenter extends MvpBasePresenter<SearchView>
    implements WeatherRepository.WeatherServiceActions {

  public void loadWeatherFromCity(String parameterSearch) {
    if (parameterSearch != null) {
      WeatherRepository weatherRepository = new WeatherRepository(this);
      weatherRepository.searchCurrentWeather(parameterSearch);
    } else {
      if (isViewAttached()) {
        getView().emptySearchParam();
      }
    }
  }

  @Override
  public void onResponse(Weather weather) {
    Bus.postSticky(weather);
    if (isViewAttached()) {
      getView().openWeatherActivity();
    }
  }

  @Override
  public void onFailure(Throwable error) {
    if (isViewAttached()) {
      getView().showError(error);
    }
  }
}
