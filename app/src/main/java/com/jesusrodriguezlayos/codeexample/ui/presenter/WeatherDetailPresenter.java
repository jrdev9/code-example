package com.jesusrodriguezlayos.codeexample.ui.presenter;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.jesusrodriguezlayos.codeexample.domain.interactor.Bus;
import com.jesusrodriguezlayos.codeexample.domain.model.Weather;
import com.jesusrodriguezlayos.codeexample.ui.view.WeatherDetailView;

public class WeatherDetailPresenter extends MvpBasePresenter<WeatherDetailView> {

  public void loadWeatherDetail() {
    Weather weather = Bus.getStickyEvent(Weather.class);
    if (isViewAttached()) {
      getView().bindWeatherDetail(weather);
    }
  }
}
