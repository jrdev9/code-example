package com.jesusrodriguezlayos.codeexample.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.jesusrodriguezlayos.codeexample.R;
import com.jesusrodriguezlayos.codeexample.domain.model.Weather;
import com.jesusrodriguezlayos.codeexample.ui.presenter.WeatherDetailPresenter;
import com.jesusrodriguezlayos.codeexample.ui.view.WeatherDetailView;

public class WeatherDetailActivity extends MvpActivity<WeatherDetailView, WeatherDetailPresenter>
    implements WeatherDetailView {

  @Bind(R.id.text_city)
  TextView textCity;
  @Bind(R.id.text_humidity)
  TextView textHumidity;
  @Bind(R.id.text_temperature)
  TextView textTemperature;
  @Bind(R.id.text_temperature_minimum)
  TextView textTemperatureMinimum;
  @Bind(R.id.text_temperature_maximum)
  TextView textTemperatureMaximum;
  @Bind(R.id.text_pressure)
  TextView textPressure;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.weather_detail_view);
    ButterKnife.bind(this);
    presenter.loadWeatherDetail();
  }

  @NonNull
  @Override
  public WeatherDetailPresenter createPresenter() {
    return new WeatherDetailPresenter();
  }

  @Override
  public void bindWeatherDetail(Weather weather) {
    textCity.setText(weather.getNameCity());
    textHumidity.setText(weather.getAtmosphericConditions().getHumidity());
    textTemperature.setText(weather.getAtmosphericConditions().getTemperature());
    textTemperatureMinimum.setText(weather.getAtmosphericConditions().getMinimumTemperature());
    textTemperatureMaximum.setText(weather.getAtmosphericConditions().getMaximumTemperature());
    textPressure.setText(weather.getAtmosphericConditions().getPreassure());
  }
}
