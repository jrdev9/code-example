package com.jesusrodriguezlayos.codeexample.ui.view;

import com.hannesdorfmann.mosby.mvp.MvpView;

public interface SearchView extends MvpView {

  void openWeatherActivity();

  void showError(Throwable error);

  void emptySearchParam();
}
