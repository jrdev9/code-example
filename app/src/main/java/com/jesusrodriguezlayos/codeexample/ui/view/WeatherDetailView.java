package com.jesusrodriguezlayos.codeexample.ui.view;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.jesusrodriguezlayos.codeexample.domain.model.Weather;

public interface WeatherDetailView extends MvpView {

  void bindWeatherDetail(Weather weather);
}
